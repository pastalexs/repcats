package com.example.appnicecats.api

import com.example.appnicecats.api.entities.CatData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiCats {
    @GET("images/search")
    fun imagesCatSearch(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Single<List<CatData>>
}