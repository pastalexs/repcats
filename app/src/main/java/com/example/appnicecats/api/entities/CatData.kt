package com.example.appnicecats.api.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "cat_table")
data class CatData(
    @PrimaryKey
    @SerializedName("id") val id:String,
    @SerializedName("url") val url:String,
    var isFavorites:Boolean=false)
