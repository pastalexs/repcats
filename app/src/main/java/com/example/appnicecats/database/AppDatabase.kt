package com.example.examplesky.database

import android.content.Context
import androidx.room.*
import com.example.appnicecats.api.entities.CatData

@Database(entities = arrayOf(CatData::class), version = 1, exportSchema = false)
abstract  class AppDatabase: RoomDatabase() {
    abstract fun catsDao(): CatsDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "cats_database"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                instance
            }
        }
    }
}