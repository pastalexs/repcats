package com.example.examplesky.database

import com.example.appnicecats.api.entities.CatData
import kotlinx.coroutines.flow.Flow


class CatsRepository(private val catsDao: CatsDao) {

    val allCatsFlow: Flow<List<CatData>> = catsDao.selectAllFlow()

    fun insert(word: CatData){
        catsDao.insert(word)
    }

    fun selectAll(): List<CatData> {
        return catsDao.selectAll()
    }

    fun delete(cat: CatData) {
         catsDao.delete(cat)
    }


}