package com.example.examplesky.database

import androidx.paging.PagingSource
import androidx.room.*
import com.example.appnicecats.api.entities.CatData
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.flow.Flow


@Dao
interface CatsDao {

    @Query("SELECT * FROM cat_table")
    fun selectAll(): List<CatData>

    @Query("SELECT * FROM cat_table")
    fun selectAllFlow(): Flow<List<CatData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cat: CatData)

    @Delete
    fun delete(cat: CatData)

}