package com.example.appnicecats.ui.cats.adapters

import com.example.appnicecats.api.entities.CatData

interface onListenerAdapter {
    fun onClickFavorites(cat: CatData, isFavorites: Boolean)
    fun downloadImage(url:String)
}