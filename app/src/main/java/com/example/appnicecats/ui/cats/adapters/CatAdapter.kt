package com.example.appnicecats.ui.cats.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.appnicecats.api.entities.CatData
import com.example.appnicecats.databinding.FragmentCatBinding
import com.google.android.material.chip.Chip
import com.squareup.picasso.Picasso


class CatAdapter(private val listener: onListenerAdapter, private val listCats: MutableList<CatData>) :
    RecyclerView.Adapter<CatViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        return CatViewHolder(
            FragmentCatBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }
    fun setListCat(listCatsNew: List<CatData>){
        listCats.clear()
        listCats.addAll(listCatsNew)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        val catItem = listCats[position]
        Picasso.with(holder.itemView.context)
            .load(catItem.url)
            .into(holder.imageView)
        holder.chip.isChecked = true
        holder.chip.setOnCheckedChangeListener { buttonView, isChecked ->
            listener.onClickFavorites(catItem, isChecked)
        }
        holder.buttonDownload.setOnClickListener {
            listener.downloadImage(catItem.url)
        }

    }

    override fun getItemCount(): Int = listCats.size
}