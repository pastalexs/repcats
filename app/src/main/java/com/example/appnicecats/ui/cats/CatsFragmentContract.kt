package com.example.appnicecats.ui.cats

import android.content.Context
import androidx.paging.PagingData
import com.example.appnicecats.api.entities.CatData
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.flow.Flow
import com.squareup.picasso.Target

interface CatsFragmentContract {
    interface View : MvpView {
    }

    interface Presenter : MvpPresenter<View> {
        fun getCatsFlowable(): Flowable<PagingData<CatData>>
        fun getCatsFlowableBd(): Flow<List<CatData>>
        fun favoritesCatInBD(cat: CatData, isFavorites: Boolean)
        fun downloadImage(url:String,context: Context):Target
    }
}