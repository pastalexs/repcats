package com.example.appnicecats.ui.cats

import android.Manifest
import android.bluetooth.BluetoothGattCharacteristic.PERMISSION_WRITE
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.appnicecats.CatApplication
import com.example.appnicecats.R
import com.example.appnicecats.api.entities.CatData
import com.example.appnicecats.ui.cats.adapters.CatAdapter
import com.example.appnicecats.ui.cats.adapters.CatPagingDataAdapter
import com.example.appnicecats.ui.cats.adapters.onListenerAdapter
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_cat_list.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import java.lang.Exception
import androidx.core.app.ActivityCompat

import android.content.pm.PackageManager
import android.widget.Toast

import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso


class CatFragment : MvpFragment<CatsFragmentContract.View, CatsPresenter>(),
    onListenerAdapter {
    private val catPagingDataAdapter = CatPagingDataAdapter(this@CatFragment)
    private val catAdapter = CatAdapter(this@CatFragment, mutableListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_cat_list, container, false)
        with(view.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = catPagingDataAdapter
            addItemDecoration(
                DividerItemDecoration(
                    view.context,
                    DividerItemDecoration.VERTICAL
                )
            )

        }
        presenter?.getCatsFlowable()?.subscribe {
            if (view.list.adapter is CatPagingDataAdapter)
                (view.list.adapter as CatPagingDataAdapter).submitData(lifecycle, it)
        }
        view.switch1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                GlobalScope.launch {
                    presenter.getCatsFlowableBd().collect {
                        MainScope().launch {
                            catAdapter.setListCat(it)
                        }
                    }
                }
                view.list.adapter = catAdapter
            } else {
                view.list.adapter = catPagingDataAdapter
            }
        }

        return view
    }

    fun checkPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_WRITE
            )
            return false
        }
        return true
    }

    override fun downloadImage(url:String){
        if(checkPermission()){
            Toast.makeText(context,"Download Image start", Toast.LENGTH_LONG).show()
            Picasso.with(requireContext()).load(url)
                .into(presenter.downloadImage(url,requireContext()))
        }
    }

    override fun createPresenter(): CatsPresenter {
        return CatsPresenter(CatApplication.instance.appComponent.manager())
    }

    override fun onClickFavorites(cat: CatData, isFavorites: Boolean) {
        presenter.favoritesCatInBD(cat, isFavorites)
    }
}