package com.example.appnicecats.ui.cats

import android.content.Context
import androidx.paging.PagingData
import com.example.appnicecats.api.entities.CatData
import com.example.examplesky.model.ManagerImp
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.flow.Flow
import android.graphics.drawable.Drawable

import android.graphics.Bitmap

import android.content.ContextWrapper
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class CatsPresenter(private val manager: ManagerImp) :
    MvpBasePresenter<CatsFragmentContract.View>(),
    CatsFragmentContract.Presenter {
    override fun getCatsFlowable(): Flowable<PagingData<CatData>> {
        return manager.getFlowCatInApi()
    }

    override fun getCatsFlowableBd(): Flow<List<CatData>> {
        return manager.getFlowCatInDataBase()
    }

    override fun favoritesCatInBD(cat: CatData, isFavorites: Boolean) {
        manager.favoritesCatInBD(cat, isFavorites)
    }

    override fun downloadImage(url: String, context: Context): Target {
        val imageDir =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        val imageName = url.split('/').last()
        val directory = File(imageDir.toURI())
        return object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: LoadedFrom?) {
                Thread {
                    val myImageFile = File(directory, imageName)
                    var fos: FileOutputStream? = null
                    try {
                        fos = FileOutputStream(myImageFile)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                        MainScope().launch {
                            Toast.makeText(context, "Download Image is success", Toast.LENGTH_LONG)
                                .show()
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                        MainScope().launch {
                            Toast.makeText(context, "Download Image is fail", Toast.LENGTH_LONG)
                                .show()
                        }
                    } finally {
                        fos?.close()

                    }
                }.start()
            }

            override fun onBitmapFailed(errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
            }
        }
    }

}