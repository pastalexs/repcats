package com.example.appnicecats.ui.cats.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.appnicecats.api.entities.CatData
import com.example.appnicecats.databinding.FragmentCatBinding
import com.google.android.material.chip.Chip
import com.squareup.picasso.Picasso


class CatPagingDataAdapter(private val listener: onListenerAdapter) :
    PagingDataAdapter<CatData, CatViewHolder>(CatDataComparator()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        return CatViewHolder(
            FragmentCatBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        val catItem = getItem(position)
        if (catItem != null) {
            Picasso.with(holder.itemView.context)
                .load(catItem.url)
                .into(holder.imageView)
            holder.chip.setOnCheckedChangeListener { buttonView, isChecked ->
                if(catItem.isFavorites!=isChecked) {
                    listener.onClickFavorites(catItem, isChecked)
                    catItem.isFavorites = isChecked
                }
            }
            holder.chip.isChecked = catItem.isFavorites
            holder.buttonDownload.setOnClickListener {
                listener.downloadImage(catItem.url)
            }

        }
    }

    class CatDataComparator : DiffUtil.ItemCallback<CatData>() {
        override fun areItemsTheSame(
            oldItem: CatData,
            newItem: CatData
        ): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(
            oldItem: CatData,
            newItem: CatData
        ): Boolean {
            return oldItem.id == newItem.id
        }
    }
}