package com.example.appnicecats.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appnicecats.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}