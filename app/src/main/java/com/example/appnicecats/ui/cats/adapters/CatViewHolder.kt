package com.example.appnicecats.ui.cats.adapters

import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnicecats.databinding.FragmentCatBinding
import com.google.android.material.chip.Chip

class CatViewHolder(binding: FragmentCatBinding) : RecyclerView.ViewHolder(binding.root) {
    val imageView: ImageView = binding.imageView
    val chip: Chip = binding.chip
    val buttonDownload: Button = binding.button


}