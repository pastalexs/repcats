package com.example.appnicecats.di

import com.example.appnicecats.CatApplication
import com.example.appnicecats.api.ApiCats
import com.example.examplesky.database.CatsRepository
import com.example.examplesky.model.DefaultManager
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule


@Component(
    modules = [AppModule::class,
        AndroidSupportInjectionModule::class]
)
interface ApplicationComponent {
    fun inject(app: CatApplication)

    fun apiCats(): ApiCats

    fun manager(): DefaultManager

    fun repository():CatsRepository
}