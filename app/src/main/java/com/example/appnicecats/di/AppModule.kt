package com.example.appnicecats.di

import android.app.Application
import com.example.appnicecats.BuildConfig
import com.example.appnicecats.api.ApiCats
import com.example.examplesky.database.AppDatabase
import com.example.examplesky.database.CatsRepository
import com.example.examplesky.model.DefaultManager
import com.example.examplesky.model.ManagerImp
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {
    @Provides
    @Singleton
    fun provideManager(
        repository: CatsRepository
    ): ManagerImp {
        return DefaultManager(repository)
    }

      @Provides
      fun provideDataBase(): AppDatabase {
          return AppDatabase.getDatabase(application)
      }

      @Provides
      fun provideWordRepository(
          dataBase: AppDatabase,
      ): CatsRepository {
          return CatsRepository(dataBase.catsDao())
      }

    @Provides
    fun provideRetrofit(): ApiCats {
        val okHttpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpBuilder.addInterceptor(httpLoggingInterceptor)
        }
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpBuilder.build())
            .build()
            .create(ApiCats::class.java)
    }
}