package com.example.appnicecats.model

import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxPagingSource
import com.example.appnicecats.CatApplication
import com.example.appnicecats.api.ApiCats
import com.example.appnicecats.api.entities.CatData

import io.reactivex.Single

import io.reactivex.schedulers.Schedulers


class CatsRxPagingSource() : RxPagingSource<Int, CatData>() {
    private val service: ApiCats = CatApplication.instance.appComponent.apiCats()
    private val repository = CatApplication.instance.appComponent.repository()
    private fun toLoadResult(
        list: List<CatData>,
        position: Int
    ): LoadResult<Int, CatData> {
        val listCatBD = repository.selectAll()
        list.forEach {
            if(listCatBD.contains(it)) {
                it.isFavorites = true
            }
        }
        return LoadResult.Page(
            data = list,
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == list.size - 1) null else position + 1
        )
    }

    override fun getRefreshKey(state: PagingState<Int, CatData>): Int? {
        return state.anchorPosition
    }

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, CatData>> {
        val position = params.key ?: 1
        return service.imagesCatSearch(position, params.loadSize)
            .subscribeOn(Schedulers.io())
            .map { toLoadResult(it, position) }
            .onErrorReturn { PagingSource.LoadResult.Error(it) }
    }
}