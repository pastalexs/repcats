package com.example.examplesky.model

import androidx.paging.PagingData
import com.example.appnicecats.api.entities.CatData
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.flow.Flow

interface ManagerImp {
    fun getFlowCatInDataBase(): Flow<List<CatData>>

    fun getFlowCatInApi(): Flowable<PagingData<CatData>>

    fun favoritesCatInBD(cat: CatData, isFavorites: Boolean)
}