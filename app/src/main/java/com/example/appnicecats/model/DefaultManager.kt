package com.example.examplesky.model

import androidx.paging.*
import androidx.paging.rxjava3.flowable
import com.example.appnicecats.api.ApiCats
import com.example.appnicecats.api.entities.CatData
import com.example.appnicecats.model.CatsRxPagingSource
import com.example.examplesky.database.CatsRepository
import io.reactivex.Observable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DefaultManager @Inject constructor(private val repository: CatsRepository) : ManagerImp {

    override fun getFlowCatInDataBase(): Flow<List<CatData>> {
        return repository.allCatsFlow
    }

    override fun getFlowCatInApi(): Flowable<PagingData<CatData>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = false,
                maxSize = 30,
                prefetchDistance = 5,
                initialLoadSize = 40
            ),
            pagingSourceFactory = {
                CatsRxPagingSource()
            }
        ).flowable
    }

    override fun favoritesCatInBD(cat: CatData, isFavorites: Boolean) {
        if (isFavorites) {
            repository.insert(cat)
        } else {
            repository.delete(cat)
        }

    }
}