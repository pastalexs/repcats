package com.example.appnicecats

import android.app.Application
import com.example.appnicecats.di.AppModule
import com.example.appnicecats.di.ApplicationComponent
import com.example.appnicecats.di.DaggerApplicationComponent


class CatApplication: Application() {

    lateinit var appComponent: ApplicationComponent
    override fun onCreate() {
        super.onCreate()
        instance = this
        setup()
    }

    fun setup() {
        appComponent = DaggerApplicationComponent.builder().appModule(AppModule(this))
            .build()

        appComponent.inject(this)
    }


    companion object {
        lateinit var instance: CatApplication private set
    }

}